FROM tomcat

RUN apt-get update -y
RUN apt-get clean -y

COPY target/scalatra-maven-prototype.war /usr/local/tomcat/webapps/app.war
EXPOSE 8080
CMD ["catalina.sh", "run"]
